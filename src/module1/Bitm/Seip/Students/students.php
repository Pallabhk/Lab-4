<?php

    namespace App\Module1\Bitm\Seip\Students;
    use PDO;
    class students{
    public $id ='';
    public $name = '';
    public $email='';
    public $pass='';
    public $gender='';

    public function setData($data = '')
    {
        if(array_key_exists('id',$data)){
            $this->id = ($data['id']);
        }
        if(array_key_exists('Name',$data)){
            $this->Name =$data['Name'];

        }
        if(array_key_exists('email',$data)){
            $this->email =$data['email'];

        }
        if(array_key_exists('pass',$data)){
            $this->pass =$data['pass'];

        }
        if(array_key_exists('gender',$data)){
            $this->gender =$data['gender'];

        }
        return $this;
    }

    public function index(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=information', 'root', '');
            $query = "SELECT * FROM `user_info` ";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
           $data= $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=information', 'root', '');
            $query = "INSERT INTO `information`.`user_info` (`id`, `Name`, `email`, `pass`, `gender`) VALUES (:a, :b,:c,:d, :e)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(array
                 (
                    ':a'=>null,
                    ':b' =>$this->Name,
                    ':c'=>$this->email,
                    ':d'=>$this->pass,
                    ':e'=>$this->gender
                 )
            );
            if($stmt){
                session_start();
                $_SESSION['message']="Successfully Submitted";
                header('location:Create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show(){

            try {
                $pdo = new PDO('mysql:host=localhost;dbname=information', 'root', '');
                $query = "SELECT * FROM `user_info` WHERE id=$this->id";
                $stmt = $pdo->prepare($query);
                $stmt->execute();
                $data= $stmt->fetch();
                return $data;
            } catch (PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
        }

}
?>